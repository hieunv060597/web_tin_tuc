(function ($) {


	document.addEventListener("DOMContentLoaded", function () {

        var menu_hd = document.querySelector(".main-header");
        var menu_top_bar = document.querySelector(".top-bar");
        var trang_thai = true;


        window.addEventListener('scroll', function () {

         

            if (window.pageYOffset > 250) {
                if (trang_thai == true) {
                    menu_hd.classList.add('fixed-header');
                    trang_thai = false;
                }
            }

            else {

                if (trang_thai == false) {
                    menu_hd.classList.remove('fixed-header');
       
                    trang_thai = true;
                }
            }
		})

		

	}, false)

    window.onload = function () {

		$(document).ready(function () {

			


            var btn_show_search_header = document.querySelector(".main-header-nav__right button");
            var btn_close_search_header = document.querySelector(".main-header .search-form-box__close button");
            var search_header_form = document.querySelector(".search-form-box");

            btn_show_search_header.addEventListener("click", function () {

                search_header_form.classList.add("show-with-only-opacity");

            });


            btn_close_search_header.addEventListener("click", function () {

                search_header_form.classList.remove("show-with-only-opacity");

            });

			var nut = document.getElementsByClassName('has-child-drop-down');

			var nd = document.getElementsByClassName('dropdown-for-child');
			for (var i = 0; i < nut.length; i++) {

				nut[i].onclick = function () {


					if (this.classList[1] == "doimau") {


						this.classList.remove("doimau");

						var dropdown = this.getAttribute('data-tq');
						var showdropdown = document.getElementById(dropdown);

						showdropdown.classList.remove('show-with-only-opacity');
					}
					else {
						for (var k = 0; k < nut.length; k++) {
							nut[k].classList.remove('doimau');
						}

						this.classList.toggle('doimau');

						var dropdown = this.getAttribute('data-tq');
					
						var showdropdown = document.getElementById(dropdown);
					
						for (var i = 0; i < nd.length; i++) {
							nd[i].classList.remove('show-with-only-opacity');
						}

						showdropdown.classList.toggle('show-with-only-opacity');
					}
				}
			}

			$('.main-page-home-page .detail-news .col-category .owl-carousel').owlCarousel({
				loop: true,
				margin: 10,
				dots: false,
				navText: ["<div class='nav-btn prev-slide'><i class = 'fa fa-angle-left'></i></div>",
					"<div class='nav-btn next-slide'><i class = 'fa fa-angle-right'></i></div>"],
				responsiveClass: true,
				responsive: {

					0: {
						items: 2,
						nav: true
					},

					578: {
						items: 2,
						nav: true
					},

					768: {
						items: 2,
						nav: true
					},

					1000: {
						items: 2,
						nav: true
					},

					1200: {
						items: 2,
						nav: true
					},

					1400: {
						items: 2,
						nav: true,
					}
				}
			})

			
			var offset = 300,
				//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
				offset_opacity = 1200,
				//duration of the top scrolling animation (in ms)
				scroll_top_duration = 700,
				//grab the "back to top" link
				$back_to_top = $('.cd-top');
			//hide or show the "back to top" link
			$(window).scroll(function () {
				($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
				if ($(this).scrollTop() > offset_opacity) {
					$back_to_top.addClass('cd-fade-out');
				}
			});
			//smooth scroll to top
			$back_to_top.on('click', function (event) {
				event.preventDefault();
				$('body,html').animate({
					scrollTop: 0,
				}, scroll_top_duration
				);
			});
            
          

        });
        
    };
 
    
 
})(jQuery);
